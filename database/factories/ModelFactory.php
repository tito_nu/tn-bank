<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'role' => 'active',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Customers::class, function ($faker) {
    return [
        'name' => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->email,
    ];
});

$factory->define(App\Transaction::class, function ($faker) {
    return [
        'deposit_amount' => $faker->randomNumber(6),
        'saldo' => $faker->randomNumber(6),
        'deposit_date_time' => $faker->dateTimeBetween('-30 days', '+30 days')
        
    ];
});
