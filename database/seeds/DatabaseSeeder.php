<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $users = [
            ['name' => 'Aditya Tito','username' => 'admin','email' => str_random(10).'@gmail.com','password' => bcrypt('admin'),'role' => 'admin','status' => 'active'],
            ['name' => 'Suseno Setyaji','username' => 'officer','email' => str_random(10).'@gmail.com','password' => bcrypt('officer'),'role' => 'officer','status' => 'active'],
        ];
        DB::table('users')->insert($users);
    }
}
