<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CustomersTableSeeder extends Seeder
{
    /**
     * Generate 10 customers data
     *
     * @return void
     */
    public function run()
    {
        factory(App\Customers::class, 10)->create()->each(
            function($cust) {
                factory(App\Transaction::class)->create(['customers_id' => $cust->id]);
            }
        );
    }
}
