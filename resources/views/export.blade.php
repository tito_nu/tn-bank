@extends('app')

@section('content')

<section class="scrollable">

<section class="bg-white panel-default">
    <ul class="breadcrumb no-border no-radius b-b b-light pull-in" style="padding-left: 25px; margin-bottom:0;" >
        <li><a href="{{ url('/') }}"><i class="fa fa-bar-chart"></i> Summary</a></li>
        <li><a href="{{ url('issue/export')}}"> Export</a></li>
    </ul>

    <header class="panel-heading icon-mute">
        <h4><i class="fa fa-print"></i>&nbsp;&nbsp;Export data</h4>
    </header>
   
    <div class="panel-body panel-default">

      {!! Form::open(['url' => 'issue/export', 'method' => 'post', 'class' => 'form-inline']) !!}

        <div class="row">
            <div class="col-lg-8">
            @include('errors.notice')
            
            <div class="form-group form-group @if($errors->has('start_date')) has-error @endif">
              <label class="sr-only">Start date</label>
              {!! Form::text('start_date', null, ['class' => 'form-control datepicker-input', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Start date issue']) !!}
              @if ($errors->has('issued')) <span class="help-block m-b-none">{{ $errors->first('start_date') }} </span> @endif
            </div>

             <div class="form-group form-group @if($errors->has('end_date')) has-error @endif">
              <label class="sr-only">End date</label>
              {!! Form::text('end_date', null, ['class' => 'form-control datepicker-input', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'End date issue']) !!}
              @if ($errors->has('issued')) <span class="help-block m-b-none">{{ $errors->first('start_date') }} </span> @endif
            </div>
           
            <button class="btn btn-info btn-sm">Export Data</button>

            </div>
        </div>

      {!! Form::Close() !!}

    </div>
      
 </section>

</section>

@stop