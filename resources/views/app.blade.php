<!DOCTYPE html>
<html lang="en" class="app">
  <head>
    <meta charset="utf-8" />
    <title>TNIS Bank</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
    {{-- <link rel="stylesheet" href="{{ asset('css/font')}}" type="text/css"> --}}
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('js/datepicker/datepicker.css') }}" type="text/css" >
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" />
    <!-- rel javascript -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/select2/select2.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('js/select2/select2.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('js/select2/theme.css') }}" type="text/css" />

    <style>
    .avatar {
    border:none;
    border-radius: 0;
    }
    .alerts{
    padding: 15px;
    margin-bottom: 20px;
    }
    .textarea {
    resize: none;
    }
    .m-15-left{
    margin-left: -15px;
    }
    .m-55-right{
    margin-right: -55px;
    }
    .btn-blue {
    color: #fff !important;
    background-color: rgb(52, 152, 219);
    border-color: rgb(52, 152, 219);
    }
    .text-lose{
    color: #95a5a6;
    }
    .text-hold{
    color: #00b0f0;
    }
    .text-canceled{
    color: #9b59b6;
    }
    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: rgb(52, 152, 219);
    border-color: rgb(52, 152, 219);
    }
    </style>
    <script type="text/javascript">
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
    placement : 'top'
    });
    });
    </script>
  </head>
  <body class="">
    <section class="vbox">
      <header class="bg-dark dk header navbar navbar-fixed-top-xs">
        <div class="navbar-header aside-md">
          <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
            <i class="fa fa-bars"></i>
          </a>
          @if(\Auth::user()->role != "admin")
          <a href="#" class="navbar-brand" data-toggle="fullscreen">Bank Officer</a>
          @else
          <a href="#" class="navbar-brand" data-toggle="fullscreen">Admin</a>
          @endif
          <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
            <i class="fa fa-cog"></i>
          </a>
        </div>

        <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="text-muted pull-left">
                {{-- <img src="{{ "http://www.gravatar.com/avatar/" . md5( strtolower( trim( \Auth::user()->email ) ) ) . "&s=" . "500" }}"> --}}
              <i class="fa fa-user"></i>
              </span>
              &nbsp;  {{ Auth::user()->name }} <b class="caret"></b>
            </a>          <ul class="dropdown-menu animated fadeInRight">

            <span class="arrow top"></span>
            <li>
              <a href="{{ url('/user/editprofile/')}}">Edit Profile</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="{{ url('logout') }}">Logout</a>
            </li>
          </ul>
        </li>
      </ul>
    </header>
    <section>
      <section class="hbox stretch">
      <section id="content">
        <section class="vbox">

          @yield('content')

        </section>
      </section>

    </section>
    <!-- /.h-strectj -->
  </section>
</section>
<!--  date picker -->
<script src="{{ asset('js/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$('.datepicker-input').datepicker()
</script>
<!-- Bootstrap -->
<script src="{{ asset('js/bootstrap.js') }}"></script>
<!-- App -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/app.plugin.js') }}"></script>
</body>
</html>
