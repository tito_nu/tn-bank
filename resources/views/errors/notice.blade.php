@if (Session::has('flash_message'))
	<div class="alerts alert-success">{{ Session::get('flash_message') }}</div>
@endif

@if (Session::has('error_message'))
	<div class="alerts alert-danger">{{ Session::get('error_message') }}</div>
@endif

@if ($errors->any())
	<div class="alerts alert-danger">
	    <button type="button" class="close" data-dismiss="alert">×</button>
	    <i class="fa fa-ban-circle"></i><strong>Ups!</strong> There was an error with your input data, please try again!.
	 </div>
@endif