@extends('app')

@section('content')

<section class="scrollable">

<section class="bg-white panel-default">

    <header class="panel-heading icon-mute">
        <h4><i class="fa fa-file-o"></i>&nbsp;&nbsp;Update user</h4>
    </header>
  <div class="row wrapper">
    <div class="col-lg-8">
      <h4>Update basic information</h4>
      <hr />
        @if (Session::has('change_profile'))
      <div class="alerts alert-success">{{ Session::get('flash_message') }}</div>
    @endif
      </div>
  </div>
   
    <div class="panel-body panel-default">

      {!! Form::model($user, ['method' => 'PATCH',  'class' => 'form-horizontal', 'action' => ['UserController@update', $user->id]]) !!}

        <div class="row">
            <div class="col-lg-8">
                @include('errors.notice')
                <div class="form-group form-group @if($errors->has('nama')) has-error @endif">
                  <label class="col-lg-2 control-label">Nama</label>
                  <div class="col-lg-10">
                      {!! Form::text('nama', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'your nama..']) !!}
                        @if ($errors->has('nama'))
                         <span class="help-block m-b-none">{{ $errors->first('nama') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('username')) has-error @endif">
                  <label class="col-lg-2 control-label">Username</label>
                  <div class="col-lg-10">
                      {!! Form::text('username', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'username..']) !!}
                        @if ($errors->has('username'))
                         <span class="help-block m-b-none">{{ $errors->first('username') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('email')) has-error @endif">
                  <label class="col-lg-2 control-label">Email</label>
                  <div class="col-lg-10">
                      {!! Form::email('email', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'you@example.com']) !!}
                        @if ($errors->has('email'))
                         <span class="help-block m-b-none">{{ $errors->first('email') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('role')) has-error @endif">
                  <label class="col-lg-2 control-label">Role</label>
                  <div class="col-lg-4">
                     {!! Form::select('role', [
                        'Admin'   => 'admin',
                        'Officer' => 'officer',
                    ], null, ['class' => 'form-control m-b']); !!}
                      @if ($errors->has('role')) <span class="help-block m-b-none">{{ $errors->first('role') }} </span> @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('status')) has-error @endif">
                  <label class="col-lg-2 control-label">status</label>
                  <div class="col-lg-4">
                     {!! Form::select('status', [
                        '1'   => 'active',
                        '2' => 'nonactive',
                    ], null, ['class' => 'form-control m-b']); !!}
                      @if ($errors->has('status')) <span class="help-block m-b-none">{{ $errors->first('status') }} </span> @endif 
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-blue btn-sm">Update user data</button>
                  </div>
                </div>

            </div>
        </div>

      {!! Form::Close() !!}


    <div class="row wrapper">
    <div class="col-lg-8">
      <h4>Change Password</h4>
      <hr />
       {{-- @if (Session::has('change_pass'))
      <div class="alerts alert-success">{{ Session::get('change_pass') }}</div>
    @endif --}}
      </div>
  </div>

      {!! Form::model($user, ['method' => 'PATCH',  'class' => 'form-horizontal', 'action' => ['UserController@changepass', $user->id]]) !!}

      <div class="row">
            <div class="col-lg-8">

     <div class="form-group form-group @if($errors->has('password')) has-error @endif">
          <label class="col-lg-2 control-label">Password</label>
          <div class="col-lg-10">
              {!! Form::password('password', ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'password...']) !!}
                @if ($errors->has('password'))
                 <span class="help-block m-b-none">{{ $errors->first('password') }} </span>
                @endif 
          </div>
        </div>

        <div class="form-group form-group @if($errors->has('password_confirmation')) has-error @endif">
          <label class="col-lg-2 control-label">Confirm Password</label>
          <div class="col-lg-10">
              {!! Form::password('password_confirmation', ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'repeat password..']) !!}
                @if ($errors->has('password_confirmation'))
                 <span class="help-block m-b-none">{{ $errors->first('password_confirmation') }} </span>
                @endif 
          </div>
        </div>

      <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
            <button class="btn btn-blue btn-sm">Change user password</button>
          </div>
        </div>

        </div>
       </div>
      {!! Form::Close() !!}

    </div>
      
 </section>

</section>

@stop