@extends('app')
@section('content')
<section class="scrollable">
  <section class="bg-white panel-default">
  <header class="panel-heading icon-mute"><h4><i class="fa fa-users"></i>&nbsp;&nbsp;List of all users</h4></header>
  @include('errors.notice')
  
  <div class="panel-body ">
    <div class="row wrapper">
      <div class="col-lg-3 m-15-left">
        <a href="{{ url('user/create') }}" class="btn btn-s-md btn-info form-control"><i class="fa fa-plus-square"></i>&nbsp; Create New User</a>
      </div>
      <div class="col-lg-3 pull-right">
        {!! Form::open(['url' => 'user/search', 'method' => 'post' ]) !!}
        <div class="input-group">
          <input type="text" name="search" class="input-sm form-control" placeholder="Search..">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="submit">Go!</button>
          </span>
        </div>
        
        {!! Form::close() !!}
      </div>
    </div>
    <div class="table-responsive">
      <table class="table b-t b-light">
        <thead>
          <tr>
            <th width="20">No</th>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if($users->count() == 0)
          <tr>
            <td colspan="6"><strong><br /><p class="text-center">No User available</p></strong></td>
          </tr>
          @else
          <?php $i = 1; ?>
          @foreach($users as $user)
          <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role }}</td>
            <td>
              
              @if(Auth::user()->role === "admin")
              <a class="btn btn-sm btn-icon btn-warning" href="{{ URL::to('user/' . $user->id . '/edit') }}"><i class="fa fa-edit fa-fw"></i></a>
              
              {!! Form::open(array('url' => 'user/' . $user->id, 'class' => 'form-horizotal', 'style' => 'display:inline;')) !!}
              {!! Form::hidden('_method', 'DELETE') !!}
              <button onclick="return confirm('are you sure delete this data?')" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o fa-fw"></i></button>
              {!! Form::close() !!}
              @endif
              
            </td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        
        <div class="col-sm-offset-4 col-sm-4 text-center">
          <div class="pagination">
            <small class="text-muted inline m-t-sm m-b-sm">showing {{ $users->count() }} of {{ $users->total() }} items</small>
          </div>
        </div>
        <div class="col-sm-4 text-right text-center-xs">
          <!-- <ul class="pagination pagination-sm m-t-none m-b-none">
            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
          </ul>
        </div> -->
        
        {!! $users->render() !!}
        
      </div>
    </footer>
  </div>
</section>
</section>
@stop