@extends('app')
@section('content')
<section class="bg-white panel-default">
	<header class="panel-heading icon-mute">
		<h4><i class="fa fa-file-o"></i>&nbsp;&nbsp;Register new Editor</h4>
	</header>
	@include('errors.notice')
	
	<section class="panel panel-default">
		<div class="panel-body">
			
				<div class="tab-pane active" id="admin">
					<div class="panel-body panel-default">
						{!! Form::open(['url' => 'user', 'method' => 'post', 'class' => 'form-horizontal']) !!}
						@include('user.form')
						{!! Form::Close() !!}
					</div>
					
				</div>
				
		</div>
	</section>
	
	
</section>
@stop