<!DOCTYPE html>
<html lang="en" class="bg-dark">
<head>
  <meta charset="utf-8" />
  <title>TN Bank Officer</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>
<body class="">
  <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xxl" style="opacity:0.9">
      <!-- <a class="navbar-brand block" style="color:white" href="index.html"><strong>O&amp;M-BSS </strong> Weekly Meeting</a> -->
      <section class="panel panel-default bg-white m-t-lg" >
        <header class="panel-heading text-center">
          <h3><strong>TN Bank </strong> <br>Cash Deposit</br></h3>
        </header>

        {!! Form::open (['url' => 'user/auth', 'method' => 'post', 'class' => 'panel-body wrapper-lg' ]) !!}

          @if (Session::has('flash_message'))
            <div class="alert alert-success">{{ Session::get('flash_message') }}</div>
          @endif

          @if (Session::has('error_message'))
            <div class="alert alert-danger">{{ Session::get('error_message') }}</div>
          @endif

          <div class="form-group">
            <label class="control-label">Username</label>
            <input type="username" name="username" placeholder="Username" class="form-control input-lg">
          </div>
          <div class="form-group">
            <label class="control-label">Password</label>
            <input type="password" name="password" id="inputPassword" placeholder="Password" class="form-control input-lg">
          </div>
          
          <!-- <a href="#" class="pull-right m-t-xs"><small>Forgot password?</small></a> -->
      
          <button type="submit" name="login" class="btn btn-primary pull-right">Sign in</button>
        {!! Form::close() !!}

      </section>
    </div>
  </section>
  <!-- footer -->
  <footer id="footer">
    <div class="text-center padder">
      <h5 style="color:white;">
       <p>&copy; 2018 TNIS Service Indonesia<p>
      </h5>
    </div>
  </footer>
  <!-- / footer -->
  
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <!-- Bootstrap -->
  <script src="{{ asset('js/bootstrap.js') }}"></script>
  <!-- App -->
  <script src="{{ asset('js/app.js') }}"></script> 
  <script src="{{ asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('js/app.plugin.js') }}"></script>

</body>
</html>