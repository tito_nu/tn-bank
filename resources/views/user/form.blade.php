

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group form-group @if($errors->has('name')) has-error @endif">
                  <label class="col-lg-2 control-label">Name</label>
                  <div class="col-lg-10">
                      {!! Form::text('nama', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'your name..']) !!}
                        @if ($errors->has('nama'))
                         <span class="help-block m-b-none">{{ $errors->first('name') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('username')) has-error @endif">
                  <label class="col-lg-2 control-label">Username</label>
                  <div class="col-lg-10">
                      {!! Form::text('username', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'username..']) !!}
                        @if ($errors->has('username'))
                         <span class="help-block m-b-none">{{ $errors->first('username') }} </span>
                        @endif 
                  </div>
                </div>

                 <div class="form-group form-group @if($errors->has('email')) has-error @endif">
                  <label class="col-lg-2 control-label">Email</label>
                  <div class="col-lg-10">
                      {!! Form::email('email', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'you@example.com']) !!}
                        @if ($errors->has('email'))
                         <span class="help-block m-b-none">{{ $errors->first('email') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('password')) has-error @endif">
                  <label class="col-lg-2 control-label">Password</label>
                  <div class="col-lg-10">
                      {!! Form::password('password', ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'password...']) !!}
                        @if ($errors->has('password'))
                         <span class="help-block m-b-none">{{ $errors->first('password') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('password_confirmation')) has-error @endif">
                  <label class="col-lg-2 control-label">Confirm Password</label>
                  <div class="col-lg-10">
                      {!! Form::password('password_confirmation', ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'repeat password..']) !!}
                        @if ($errors->has('password_confirmation'))
                         <span class="help-block m-b-none">{{ $errors->first('password_confirmation') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('role')) has-error @endif">
                  <label class="col-lg-2 control-label">Role</label>
                  <div class="col-lg-4">
                     {!! Form::select('role', [
                        'viewer'   => 'viewer',
                        'operator' => 'operator',
                        'member' => 'member',
                        'admin'    => 'admin',
                    
                    ], null, ['placeholder'  => 'Pick a Role...', 'class' => 'form-control m-b']); !!}
                      @if ($errors->has('role')) <span class="help-block m-b-none">{{ $errors->first('role') }} </span> @endif 
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-info btn-sm">Submit Data</button>
                    <input type="reset" class="btn btn-default btn-sm" value="cancel" />
                  </div>
                </div>

            </div>
        </div>
        

