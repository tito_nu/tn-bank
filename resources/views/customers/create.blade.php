@extends('app')

@section('content')

<section class="bg-white panel-default">

    <header class="panel-heading icon-mute">
        <h4><i class="fa fa-file-o"></i>&nbsp;&nbsp;Register new Customers</h4>
    </header>

    @include('errors.notice')
   
    <div class="panel-body panel-default">

      {!! Form::open(['url' => 'customers', 'method' => 'post', 'class' => 'form-horizontal']) !!}

        @include('customers.form')

      {!! Form::Close() !!}

    </div>
      
 </section>


@stop