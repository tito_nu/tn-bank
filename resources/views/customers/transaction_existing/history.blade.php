@extends('app')

@section('content')

<section class="scrollable">

      <section class="bg-white panel-default">

        <header class="panel-heading icon-mute"><h4><i class="fa fa-users"></i>&nbsp;&nbsp;List of all customers</h4></header>

        @include('errors.notice')
       

        <div class="panel-body ">

          <div class="row wrapper">


                    <div class="col-lg-3 m-15-left">
                       <a href="{{ url('customers/create') }}" class="btn btn-s-md btn-success form-control"><i class="fa fa-plus-square"></i>&nbsp; Register New Customers</a>
                    </div>
                 
                    <div class="col-lg-3 pull-right">
                    {!! Form::open(['url' => 'admin/search', 'method' => 'post' ]) !!}
                      <div class="input-group">
                        <input type="text" name="search" class="input-sm form-control" placeholder="Search..">
                        <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="submit">Go!</button>
                        </span>
                      </div>
                     
                    {!! Form::close() !!}
                  </div>


              </div>

              <div class="table-responsive">
                    <table class="table b-t b-light">
                      <thead>
                        <tr>
                          <th width="20">No</th>
                          <th>Name</th>
                          <th>Phone Number</th>
                          <th>Email</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($customers->count() == 0)
                        <tr>
                            <td colspan="6"><strong><br /><p class="text-center">No Customer available</p></strong></td>
                        </tr>
                      @else
                        <?php $i = 1; ?>
                        @foreach($customers as $customer)
                        <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $customer->name }}</td>
                          <td>{{ $customer->phone_number }}</td>
                          <td>{{ $customer->email }}</td>
                          <td>
                          {{-- deposit --}}
                            <a class="btn btn-sm btn-icon btn-success" href="{{ URL::to('customers/' . $customer->id . '/edit') }}"><i class="fa fa-money fa-fw"></i></a>
                          {{-- history --}}
                            <a class="btn btn-sm btn-icon btn-info" href="{{ URL::to('customers/' . $customer->id . '/edit') }}"><i class="fa fa-history fa-fw"></i></a>
                          {{-- edit --}}
                           <a class="btn btn-sm btn-icon btn-warning" href="{{ URL::to('customers/' . $customer->id . '/edit') }}"><i class="fa fa-edit fa-fw"></i></a>
                          {{-- delete --}}
                          {!! Form::open(array('url' => 'customers/' . $customer->id, 'class' => 'form-horizotal', 'style' => 'display:inline;')) !!}
                              {!! Form::hidden('_method', 'DELETE') !!}
                              <button onclick="return confirm('are you sure delete this data?')" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o fa-fw"></i></button>
                          {!! Form::close() !!}
                          </td> 
                        </tr>
                        @endforeach
                       @endif
                      </tbody>
                    </table>
                  </div>

                  <footer class="panel-footer">
                    <div class="row">
                      
                      <div class="col-sm-offset-4 col-sm-4 text-center">
                        <div class="pagination">
                            <small class="text-muted inline m-t-sm m-b-sm">Showing  {{ $customers->count() }} of {{ $customers->total() }} customers</small>
                          </div>
                      </div>
                      <div class="col-sm-4 text-right text-center-xs">                
                       
                          {!! $customers->render() !!}
                     
                    </div>
                  </footer>

        </div>
    </section>

 </section>


@stop