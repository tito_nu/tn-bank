@extends('app')

@section('content')

<section class="bg-white panel-default">

    <header class="panel-heading icon-mute">
        <h4><i class="fa fa-file-o"></i>&nbsp;&nbsp;Adding deposit to <b>{{$customers->name}}</b></h4>
    </header>

    @include('errors.notice')
   
    <div class="panel-body panel-default">

      {!! Form::open(['action' => ['TransactionController@store', $customers->id] , 'method' => 'post', 'class' => 'form-horizontal']) !!}

        @include('customers.transaction_existing.form_add_deposit')

      {!! Form::Close() !!}

    </div>
      
 </section>


@stop