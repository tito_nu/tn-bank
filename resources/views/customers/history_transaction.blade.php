@extends('app')

@section('content')

<section class="scrollable">

      <section class="bg-white panel-default">

        <header class="panel-heading icon-mute"><h4><i class="fa fa-users"></i>&nbsp;&nbsp;Transaction history of <b>{{$customer->name}}</b></h4></header>

        @include('errors.notice')
       

        <div class="panel-body ">

          <div class="row wrapper">
                    <div class="col-lg-3 m-15-left">
                       <a href="{{ url('transaction/' . $customer->id . '/add_deposit') }}" class="btn btn-s-md btn-success form-control"><i class="fa fa-plus-square"></i>&nbsp; Added New Deposit</a>
                    </div>
              </div>

              <div class="table-responsive">
                    <table class="table b-t b-light">
                      <thead>
                        <tr>
                          <th width="20">No</th>
                          <th>Deposit Amount</th>
                          <th>Saldo</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($transactions->count() == 0)
                        <tr>
                            <td colspan="6"><strong><br /><p class="text-center">No transaction available</p></strong></td>
                        </tr>
                      @else
                        <?php $i = 1; ?>
                        @foreach($transactions as $transaction)
                        <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $transaction->deposit_amount }}</td>
                          <td>{{ $transaction->saldo }}</td>
                          <td>{{ $transaction->deposit_date_time }}</td>
                        </tr>
                        @endforeach
                       @endif
                      </tbody>
                    </table>
                  </div>

                  <footer class="panel-footer">
                    <div class="row">
                      
                      <div class="col-sm-offset-4 col-sm-4 text-center">
                        <div class="pagination">
                            <small class="text-muted inline m-t-sm m-b-sm">Showing  {{ $transactions->count() }} of {{ $transactions->total() }} Transaction</small>
                          </div>
                      </div>
                      <div class="col-sm-4 text-right text-center-xs">                
                       
                          {!! $transactions->render() !!}
                     
                    </div>
                  </footer>

        </div>
    </section>

 </section>


@stop