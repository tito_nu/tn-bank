        <div class="row">
            <div class="col-lg-8">
                <div class="form-group form-group @if($errors->has('name')) has-error @endif">
                  <label class="col-lg-2 control-label">Name</label>
                  <div class="col-lg-10">
                      {!! Form::text('name', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'your name..']) !!}
                        @if ($errors->has('name'))
                         <span class="help-block m-b-none">{{ $errors->first('name') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('phone_number')) has-error @endif">
                  <label class="col-lg-2 control-label">Phone number</label>
                  <div class="col-lg-10">
                      {!! Form::number('phone_number', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20]) !!}
                        @if ($errors->has('phone_number'))
                         <span class="help-block m-b-none">{{ $errors->first('phone_number') }} </span>
                        @endif 
                  </div>
                </div>

                 <div class="form-group form-group @if($errors->has('email')) has-error @endif">
                  <label class="col-lg-2 control-label">Email</label>
                  <div class="col-lg-10">
                      {!! Form::email('email', null, ['class' => 'form-control', 'cols' => 20, 'rows' => 20, 'placeholder' => 'you@example.com']) !!}
                        @if ($errors->has('email'))
                         <span class="help-block m-b-none">{{ $errors->first('email') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('deposit_amount')) has-error @endif">
                  <label class="col-lg-2 control-label">Deposit Amount</label>
                  <div class="col-lg-10">
                    <div class="input-group m-b">
                      <span class="input-group-addon">Rp</span>
                      {!! Form::number('deposit_amount', null, [ 'class' => 'form-control' ] ) !!}
                      <span class="input-group-addon">.00</span>
                    </div> 
                  </div>
                </div>

                <div class="form-group form-group @if($errors->has('date')) has-error @endif">
                  <label class="col-lg-2 control-label">Date and time</label>
                  <div class="col-lg-10">
                     {!! Form::text('date', $current_time, [ 'readonly' , 'class' => 'form-control'] ) !!}
                        @if ($errors->has('date'))
                         <span class="help-block m-b-none">{{ $errors->first('date') }} </span>
                        @endif 
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-info btn-sm">Submit Data</button>
                    <input type="reset" class="btn btn-default btn-sm" value="cancel" />
                  </div>
                </div>

            </div>
        </div>
        

