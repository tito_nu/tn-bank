<!DOCTYPE html>
<html>
<head>
    <title>FormValidation demo</title>
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.css"/>
</head>
<body>
    <div class="container">
        <div class="row">
            <h2>Form data</h2>
            <hr/>
            <p>This is a simple page showing the data you have just submitted</p>
            <pre><?php print_r($_POST['textbox']); ?></pre>
        </div>
    </div>
</body>
</html>

<?php
//echo $_POST['textbox'][0];
$file = "lintasnormd.zone";
$file_tmp = "lintasnormd.tmp";

if (file_exists($file)){
    $reading = fopen($file, 'r') or die('Cannot read file:  '.$file);
    $writing = fopen($file_tmp, 'w') or die('Cannot create file:  '.$file_tmp);

    $replaced = false;

    $t_data = file($file);
    $t_data[2] = trim($t_data[2]);
    $edit_prev = intval((substr($t_data[2], 8, -6)))+1;
    $edit = sprintf("%02d", $edit_prev);
    echo $edit_prev."<br>";
    $sps = str_repeat(" ", 22);
    $finish = !empty($_POST["finish"]);
    $pass = $finish;
    //echo "$data";

    while (!feof($reading))
        {
            $line = fgets($reading);
            if (stristr($line,(strstr($line, " ; serial", true))) && isset($finish)) {
                $line = $sps.date("Ymd").$edit." ; serial\n";
                $replaced = true;
            }
            fwrite($writing, $line);
        }
    
    fclose($reading); fclose($writing);
    if ($replaced) {
      rename($file_tmp, $file);
        } else {
          unlink($file_tmp);
        }

    } 
header('Location:http://192.168.10.135//lintasarta/index.php?finish=true');
?>
