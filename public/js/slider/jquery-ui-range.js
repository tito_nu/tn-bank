$(document).ready(function() {
                var min_rent = 200000,
                max_rent = 1500000;
                $('#slider-price-low').val(min_rent);
                $('#slider-price-high').val(max_rent);
 
                $('#slider-price').slider({
                    orientation: 'horizontal',
                    range: true,
                    animate: 200,
                    min: 0,
                    max: 3000000,
                    step: 1000,
                    value: 0,
                    values: [min_rent, max_rent],
                    slide: function(event,ui) {
                        $('#slider-price-low').val(ui.values[0]);
                        $('#slider-price-high').val(ui.values[1]);
                    }
                });
 
                $('#slider-price-low').change(function () {
                    var low = $('#slider-price-low').val(),
                    high = $('#slider-price-high').val();
                    low = Math.min(low, high);
                    $('#slider-price-low').val(low);
                    $('#slider-price').slider('values', 0, low);
                });
 
                $('#slider-price-high').change(function () {
                    var low = $('#slider-price-low').val(),
                    high = $('#slider-price-high').val();
                    high = Math.max(low, high);
                    $('#slider-price-high').val(high);
                    if(high > 3000000) {
                        $('#slider-price-high').val('3000000');
                    }
                    $('#slider-price').slider('values', 1, high);
                });
            });
                    function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
                            