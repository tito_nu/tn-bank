<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model 
{

    /**
     * The database table Customers used by the model.
     *
     * @var string
     */
    protected $table = 'Customers';
    protected $guarded = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'phone_number', 'email'];

    /**
     * The relation one to many to transaction table.
     *
     * @return \App\Transaction
     */
    public function transaction()
    {
        return $this->hasMany('\App\Transaction');
    }
    
    public $timestamps = false;

}
