<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
     /**
     * The database table Customers used by the model.
     *
     * @var string
     */
    protected $table = 'Transaction';
    protected $guarded = ['id'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['deposit_amount', 'saldo', 'deposit_date_time'];

    /**
     * The relation many to one or belongs to customers table.
     *
     * @return \App\Customers
     */
 	public function customers()
    {
        return $this->belongsto('\App\Customers');
    }
  
    public $timestamps = false;
}
