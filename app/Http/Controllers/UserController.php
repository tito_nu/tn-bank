<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Display a listing of the Users.
     *
     * @return \resource\view\user\index.blade.php
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return \resource\view\user\create.blade.php
     */
    public function create()
    {
        return view('user.create', compact('section'));
    }

    /**
     * Store a newly created users datain storage.
     *
     * @param  Request  $request
     * @return \resource\view\user\index.blade.php
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'nama'     => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'role'     => 'required',
            'status'   => 'required',
        ]);

        User::create([
            'nama'     => $request['nama'],
            'username' => $request['username'],
            'email'    => $request['email'],
            'password' => bcrypt($request['password']),
            'role'     => $request['role'],
            'status'   => $request['status'],
        ]);

        \Session::flash('flash_message', "new user has been created!");
        return redirect('user');
    }

    /**
     * search data by query in every field of issue
     * @return \resource\view\user\index.blade.php
     */
    public function search(Request $request)
    {
        $users = User::where('nama', 'like', "%$request->search%")
            ->orWhere('username', 'like', "%$request->search%")
            ->orWhere('role', 'like', "%$request->search%")
            ->orWhere('email', 'like', "%$request->search%")
            ->paginate(10);

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for editing the user data from user list action.
     *
     * @param  int  $id
     * @return \resource\view\user\edit.blade.php
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified user data in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \resource\view\user\index.blade.php
     */
    public function update(Request $request, $id)
    {
        /*return $request->all();*/
        $validator = $this->validate($request, [
            'nama'     => 'required|max:255',
            'username' => 'required|max:255 | unique:users,username,' . $id,

        ]);

        // update user data
        $user = User::findOrFail($id);
        $user->update([
            'nama'     => $request['nama'],
            'username' => $request['username'],
            'email'    => $request['email'],

        ]);

        \Session::flash('flash_message', "Data user berhasil diperbaharui!");

        return back();
    }

    /**
     * Update the password user data in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \resource\view\user\index.blade.php
     */
    public function changepass(Request $request, $id)
    {
        // validate data
        $validator = $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        // update user password
        $user = User::findOrFail($id);
        $user->update([
            'password' => bcrypt($request['password']),
        ]);

        \Session::flash('change_pass', "Password telah berhasil diperbaharui!");

        return back();
    }

     /**
      * Show the form for editing the own user data when logged in.
      *
      * @param  int  $id
      * @return \resource\view\user\editprofile.blade.php
      */
    public function editprofile()
    {
        $user = User::findOrFail(\Auth::user()->id);
        return view('user.editprofile', compact('user'));
    }

     /**
      * Update password for user data when logged in.
      *
      * @param  int  $id
      * @param  Request  $request
      * @return \resource\view\user\index.blade.php
      */
    public function userchangepassword(Request $request, $id)
    {

        $validator = $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        $user = User::findOrFail($id);

        $user->update([
            'password' => bcrypt($request['password']),
        ]);

        \Session::flash('change_pass', "User password has been changed!");

        return back();
    }

    /**
     * Remove the user data from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $user       = User::find($id);
        $id_pemilik = $user->pemilik->id;
        $user->delete();
        $user->pemilik()->delete();
        $user->pemilik->mobil()->delete();
        $user->pemilik->fitur()->delete();
        $user->pemilik->transaksi()->delete();

        \Session::flash('flash_message', "Data Pemilik Telah Berhasil Dihapus");

        return back();
    }

     /**
      * Show the form login.
      *
      * @return \resource\view\user\login.blade.php
      */
    public function getLogin()
    {
        return view('user.login');
    }

    /**
     * Authenticate user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function authenticate(Request $request)
    {
        // if auth user based on her role
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            if (\Auth::user()->role == "officer") {
                return redirect('customers');
            } elseif (\Auth::user()->role == "admin") {
                return redirect('user');
            }

        } else {
            \Session::flash('error_message', "username or password is incorrect");
            return back();
        }
    }

    /**
      * Show the logout page.
      *
      * @return \resource\view\user\login.blade.php
      */
    public function getLogout()
    {
        Auth::logout();
        \Session::flash('flash_message', "You have succesfully logging out");
        return redirect('/login');
    }

}
