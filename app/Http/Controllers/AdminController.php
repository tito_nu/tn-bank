<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of Admin members.
     *
     * @return \views\admin\index.blade.php
     */
    public function index()
    {
        $users = \App\User::where('role', '=', 'admin')->paginate(10);
        return view('admin.index', compact('users'));
    }

    /**
     * Show the form for creating a new Admin member.
     *
     * @return \views\admin\create.blade.php
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \views\admin\index.blade.php
     */
    public function store(Request $request)
    {
        // validate input
        $validator = $this->validate($request,[
            'username' => 'required|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'nama'     => 'required',
            'email'    => 'required|email',
        ]);

        $user = new \App\User;
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->name = $request->input('nama');
        $user->password = bcrypt($request->input('password'));
        $user->role = "admin";
        $user->save();

        \Session::flash('flash_message', "Admin telah berhasil dibuat");
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::findOrFail($id);
        return view('admin.edit', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $user = \App\User::find($id);
        $user->delete();

        // redirect
        \Session::flash('flash_message', "Admin telah berhasil dihapus");

        return back();
    }
}
