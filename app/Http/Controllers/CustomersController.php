<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = \App\Customers::paginate(10);
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current_time = Carbon::now('Asia/Jakarta')->toDateTimeString();
        return view('customers.create', compact('current_time'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate input
        $validator = $this->validate($request, [
            'name'           => 'required',
            'phone_number'   => 'required',
            'email'          => 'required|email',
            'deposit_amount' => 'required',
        ]);
        $customers               = new \App\customers;
        $customers->name         = $request->input('name');
        $customers->phone_number = $request->input('phone_number');
        $customers->email        = $request->input('email');
        $customers->save();

        $transaction                    = new \App\transaction;
        $transaction->customers_id      = $customers->id;
        $transaction->deposit_amount    = $request->input('deposit_amount');
        $transaction->deposit_date_time = $request->input('date');
        $transaction->saldo             = $request->input('deposit_amount');
        $transaction->save();

        \Session::flash('flash_message', "New customer data has been succesfully added");
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //showing customer history transaction
        $customer = \App\Customers::findOrFail($id);
        $transactions = \App\transaction::where('customers_id','=', $id)->paginate(10);
        return view('customers.history_transaction', compact('transactions', 'customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = \App\customers::findOrFail($id);
        return view('editor.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $customers = \App\customers::find($id);
        $customers->delete();

        // redirect
        \Session::flash('flash_message', "User telah berhasil dihapus");

        return back();
    }

    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function sendEmailReminder(Request $request, $id)
    {
        $cust = \App\Customers::with('transaction')->findOrFail($id);
        $max_id = \App\transaction::where('customers_id', '=', $id)->max('id');
        $transaction = \App\transaction::where('customers_id', '=', $id)->where('id', '=', $max_id)->first();

        Mail::send('emails.reminder', ['cust' => $cust, 'transaction' => $transaction], function ($m) use ($cust) {
            $m->from('tnbank@app.com', 'noreply@app.com');
            $m->to($cust->email, $cust->name)->subject('Deposit Status');
        });
    }
}
