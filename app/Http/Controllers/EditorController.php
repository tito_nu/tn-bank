<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EditorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::where('role', '=', 'editor')->paginate(10);
        return view('editor.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('editor.create');
    }
    public function create_editor()
    {
        return view('editor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate input
        $validator = $this->validate($request,[
            'username' => 'required|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'nama'     => 'required',
            'email'    => 'required|email',
        ]);

        $user = new \App\User;
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->nama = $request->input('nama');
        $user->password = bcrypt($request->input('password'));
        $user->role = "editor";
        $user->save();

        \Session::flash('flash_message', "editor telah berhasil dibuat");
        return redirect('editor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::findOrFail($id);
        return view('editor.edit', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $user = \App\User::find($id);
        $user->delete();
        $user->penyewa()->delete();

        // redirect
        \Session::flash('flash_message', "User telah berhasil dihapus");

        return back();
    }
}
