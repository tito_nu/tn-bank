<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail; 

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \resource\view\customers\index.blade.php
     */
    public function index()
    {
        $customers = \App\Customers::paginate(10);
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for adding new transaction for existing customers.
     *
     * @return \resource\view\customers\transaction_existing\add_deposit.blade.php
     */
    public function add_deposit($id)
    {
        $customers    = \App\Customers::findOrFail($id);
        $current_time = Carbon::now('Asia/Jakarta')->toDateTimeString();
        return view('customers.transaction_existing.add_deposit', compact('customers', 'current_time'));
    }

    /**
     * Store deposit for existing customer
     *
     * @param  $request $id
     * @return \resource\view\customers\index.blade.php
     */
    public function store(Request $request, $id)
    {
        // validate input
        $validator = $this->validate($request, [
            'deposit_amount' => 'required',
        ]);

        // Get last saldo from the latest transaction
        $max_id = \App\transaction::where('customers_id', '=', $id)->max('id');
        $last_saldo = \App\transaction::select('saldo')->where('customers_id', '=', $id)->where('id', '=', $max_id)->first();

        // Check whether the user has previous savings or not
        if (is_null($last_saldo)) {
            $saldo = $request->input('deposit_amount');
        } else {
            $saldo = $request->input('deposit_amount') + $last_saldo->saldo;
        }

        // store transaction
        $transaction                    = new \App\transaction;
        $transaction->customers_id      = $id;
        $transaction->deposit_amount    = $request->input('deposit_amount');
        $transaction->deposit_date_time = $request->input('date');
        $transaction->saldo             = $saldo;
        $transaction->save();

        // Get customer data and transactions
        $cust = \App\Customers::with('transaction')->findOrFail($id);
        $transaction = \App\transaction::where('customers_id', '=', $id)->where('id', '=', $max_id)->first();

        // send email notification to customer
        Mail::send('emails.reminder', ['cust' => $cust, 'transaction' => $transaction], function ($m) use ($cust) {
            $m->from('tnbank@app.com', 'noreply@app.com');
            $m->to($cust->email, $cust->name)->subject('Deposit Status');
        });

        \Session::flash('flash_message', "New customer data transaction has been succesfully added");
        return redirect('customers');
    }

}
