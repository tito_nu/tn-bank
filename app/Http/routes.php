<?php
 /*authenticate user */
Route::get('/', function(){
	return redirect('/login');
	});
Route::get('/login', 'UserController@getLogin');
Route::post('user/auth', 'UserController@authenticate');
Route::get('logout', 'UserController@getLogout');


Route::group(['middleware' => 'auth'], function () {
	Route::resource('admin', 'AdminController');
	Route::get('user/editprofile/', 'UserController@editprofile');
	Route::patch('user/editprofile/changepass/{id}', 'UserController@userchangepassword');
	
	/*Officer routes ~ */
	Route::group(['middleware' => 'role:officer'], function () {
		Route::resource('customers', 'CustomersController');
		Route::get('transaction/{id}/add_deposit', 'TransactionController@add_deposit');
		Route::post('transaction/{id}/add_deposit', 'TransactionController@store');
	});

	/*admin routes ~ */
	Route::group(['middleware' => 'role:admin'], function () {
		Route::resource('user', 'UserController');
		Route::resource('/officer','UserController@officer_list'); 
   
		Route::get('/admin','UserController@admin_list');
		Route::get('/officer','UserController@officer_list');

		Route::post('user/search/', 'UserController@search');
		Route::post('admin/search/', 'UserController@searchAdmin');

		Route::patch('user/changepass/{id}', 'UserController@changepass');
		
	});
	
});





