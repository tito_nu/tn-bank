<?php

namespace App;
use \App\Section;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama', 'username', 'email', 'password', 'role', 'status', 'section_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    
    // checking user permission

     // has one pemilik
    public function pemilik()
    {
        return $this->hasOne('\App\Pemilik');
    }

    // has one penyewa
    public function penyewa()
    {
        return $this->hasOne('\App\Penyewa');
    }

    
    public function hasRole($name)
    {
        if ($this->role === $name) {
            return true;
        }
        return false;
    }

   /* public function mobil(){
        $this->hasManyThrough('\App\Pemilik', '\App\Mobil');
    }*/

}
