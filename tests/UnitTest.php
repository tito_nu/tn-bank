<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnitTest extends TestCase
{
	use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_create_login_officer()
    {
    	factory(App\User::class)->create([
	        'name' => 'unit test officer',
	        'username' => 'testing_officer',
	        'email' => 'testingofficer@gmail.com',
	        'password' => bcrypt('testing_officer'),
	        'role' => 'officer',
	        'status' => 'active',
	        'remember_token' => str_random(10),
      		]);

    	$this->seeInDatabase('users', ['email' => 'testingofficer@gmail.com']);

        $this->visit('/login')
            ->submitForm('login', ['username' => 'testing_officer', 'password' => 'testing_officer'])
            ->see('List of all customers');
    }

    public function test_create_login_admin()
    {
    	factory(App\User::class)->create([
	        'name' => 'unit test admin',
	        'username' => 'testing_admin',
	        'email' => 'testingadmin@gmail.com',
	        'password' => bcrypt('testing_admin'),
	        'role' => 'admin',
	        'status' => 'active',
	        'remember_token' => str_random(10),
      		]);

    	$this->seeInDatabase('users', ['email' => 'testingadmin@gmail.com']);

        $this->visit('/login')
            ->submitForm('login', ['username' => 'testing_admin', 'password' => 'testing_admin'])
            ->see('Admin');
    }

    public function test_create_customer()
    {
    	
        // factory(App\Transaction::class, 1)->create();
/*
        factory(App\Customers::class, 2)->create()->each(function($u) {
		    $u->transation->save(factory(App\Transaction::class)->make());
		  });
*/
        factory(App\Customers::class, 2)->create()->each(
            function($cust) {
                factory(App\Transaction::class)->create(['customers_id' => $cust->id]);
            }
        );
        
    	$this->seeInDatabase('Transaction', ['id' => '10']);

    }
}
