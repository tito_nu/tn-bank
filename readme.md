
# Cash Deposit Backend

This application is made to facilitate bank officer to save the data deposit

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installation
Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.1)

Clone the repository

```
git clone https://tito_nu@bitbucket.org/tito_nu/tn-bank.git
```
Switch to the repo folder

```
cd tn-bank
```
Install all the dependencies using composer

```
composer install
```
Copy the example env file and make the required configuration changes in the .env file

```
cp .env.example .env
```
Generate a new application key

```
php artisan key:generate
```
Run the database migrations (Set the database connection in .env before migrating)

```
php artisan migrate
```

Start the local development server

```
php artisan serve
```
You can now access the server at http://localhost:8000

TL;DR command list

```
git clone https://tito_nu@bitbucket.org/tito_nu/tn-bank.git
cd tn-bank
composer install
cp .env.example .env
php artisan key:generate
php artisan migrate
```
Make sure you set the correct database connection information before running the migrations Environment variables
```
php artisan migrate
php artisan serve
```
## Database seeding

**Populate the database with seed data with relationships which includes users and customers. This can help you to quickly start testing for couple a frontend and start using it.**

Open the DatabaseSeeder and set the property values as per your requirement

    database/seeds/DatabaseSeeder.php

Run the database seeder and you're done to insert 2 new user data, which one of them is admin and bank officer. which will be used for login later

    php artisan db:seed
(Optional) if you want to generate the 10 customer data along with the transactions, you can run the script as follows

    php artisan db:seed --class=CustomersTableSeeder

## Unit Testing

UnitTes file is where unit testing is configured

    tests/Unit.php

here is a script to run unit testing

    phpunit

    
# Code overview
## Folders

- `app` - Contains all the Eloquent models
- `config` - Contains all the application configuration files
- `database/factories` - Contains the model factory for all the models
- `database/migrations` - Contains all the database migrations
- `database/seeds` - Contains the database seeder
- `routes` - Contains all the api routes defined in api.php file
- `tests` - Contains all the application tests
- `tests/Feature/Api` - Contains all the api tests

## Environment variables

- `.env` - Environment variables can be set in this file

----------
