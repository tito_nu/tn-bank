-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 19, 2016 at 04:05 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upload_image`
--

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `url` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `script` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `name`, `start_date`, `end_date`, `url`, `created_at`, `updated_at`, `script`) VALUES
(13, 'ini iklan', '2016-10-01', '2016-10-02', '1476761513.jpg', '2016-10-18 03:31:53', '2016-10-17 20:31:53', ''),
(14, 'itu iklan', '2016-10-02', '2016-10-03', '1476701815.jpg', '2016-10-17 03:56:55', '2016-10-17 03:56:55', ''),
(15, 'l', '2016-10-11', '2016-10-05', '1476703473.png', '2016-10-17 04:24:33', '2016-10-17 04:24:33', ''),
(16, 'blabla', '2016-10-18', '2016-10-20', '1476759932.jpg', '2016-10-17 20:05:33', '2016-10-17 20:05:33', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `email`, `password`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(50, 'tito', 'admin', 'titocak@gmail.com', '$2y$10$goDa2qzFPhLlBkmMYT7mOePFBwOZ5y7Afh3ut/VsSU6iEkAtp.kRy', 'admin', 1, 'uauSwNIRjhCaxu1xVz0JNygeaEs2oJy27v0u5Cgq4lRzUW5LyK9AHugbgxKq', '2016-01-10 08:09:49', '2016-10-18 03:17:16'),
(62, 'hasan', 'hasan', 'hasan@datacomm.co.id', '$2y$10$WQRfmworT1KigpBOvg6GguNLpUZB0oeBb9bjzG/5rqlr4sUNgD.dG', 'admin', 1, 'kvsH3PtESWwdUGXxYlMQo4epEHhRltOzcd4VoYw59kKLFbvvdkE0UnBFtN8t', '2016-10-14 00:49:52', '2016-10-14 01:13:32'),
(63, 'editor', 'editor', 'editor@gmail.com', '$2y$10$XuBMRWPj/IMt2d8Jb5my5OFsQxrii0GihqdkamyX/AXxWXCJB351i', 'editor', 1, 'iCgitwnPMlKfYtpzGGJEXd5sJTQpnGqNzo7ar6XaAj63t6OKOwSi8xgk9nab', '2016-10-14 01:13:27', '2016-10-17 04:38:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
